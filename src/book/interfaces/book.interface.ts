import { HttpStatus } from "@nestjs/common";

export interface BookI {
    titre: string;
    resume?: string;
    author_id?: number;
    category_id?: number;
}

export interface CustomResponseI {
    message: string;
    content?: any | any[];
    status: HttpStatus;
}