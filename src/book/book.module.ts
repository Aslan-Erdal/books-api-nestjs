import { Module } from '@nestjs/common';
import { BookService } from './services/book.service';
import { BookController } from './controller/book.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookEntity } from './entities/book.entity';
import { AuthorModule } from 'src/author/author.module';
import { AuthorEntity } from 'src/author/entities/author.entity';
import { CategoryEntity } from 'src/category/entities/category.entity';
import { BorrowModule } from 'src/borrow/borrow.module';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([BookEntity, AuthorEntity, CategoryEntity]), AuthorModule, BorrowModule, UserModule],
  controllers: [BookController],
  providers: [BookService]
})
export class BookModule {}
