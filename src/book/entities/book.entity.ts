import { AuthorEntity } from "src/author/entities/author.entity";
import { BorrowEntity } from "src/borrow/entities/borrow.entity";
import { CategoryEntity } from "src/category/entities/category.entity";
import { TimestampEntities } from "src/generics/timestamp.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('book')
export class BookEntity extends TimestampEntities {

    @PrimaryGeneratedColumn({
        type: "int",
        name: 'book_id',
        primaryKeyConstraintName: 'book_id'
    })
    id: number;

    @Column({ name: 'titre', nullable: false, })
    titre: string;

    @Column({
        name: 'resume', nullable: true
    })
    resume: string;

    @ManyToOne( _ => AuthorEntity, (author) => author.livres, {
        cascade: ['insert', 'update'],
        nullable: true,
    })
    @JoinColumn({ name: 'author_id' })
    author: AuthorEntity

    @ManyToMany(
        () => CategoryEntity,
        categories => categories.books,
        {
            nullable: true,
        }
    )
    @JoinTable({
        name: 'book_category',
        joinColumn: {
            name: 'book_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'category_id',
            referencedColumnName: 'id',
        }
    })
    categories: CategoryEntity[];

    @OneToMany(() => BorrowEntity, (borrow) => borrow.books)
    borrow: BorrowEntity[]

}
