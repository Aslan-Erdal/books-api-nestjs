import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, ParseIntPipe, Put, HttpException } from '@nestjs/common';
import { BookService } from '../services/book.service';
import { CreateUpdateBookDto } from '../dto/create-update-book.dto';
import { CustomResponseI } from '../interfaces/book.interface';

@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Post()
  public async create(@Body() createBookDto: CreateUpdateBookDto): Promise<CustomResponseI> {
    const createdBook = await this.bookService.create(createBookDto);
    return {
      message: `Le livre avec le '${createdBook.titre}' a été crée avec succès!`,
      content: createdBook,
      status: HttpStatus.CREATED,
    }
  }

  @Get()
  public async findAll(): Promise<CustomResponseI> {
   const allBooks = await this.bookService.findAll();
   return {
    message: 'Les livres ont été récupérés avec succès!',
    content: allBooks,
    status: HttpStatus.OK,
  }
   
  }

  @Get(':id')
  public async findOne(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {
    const foundBook = await this.bookService.findOne(id);

    if (!foundBook) {
      throw new HttpException('Le livre est introuvable.', HttpStatus.NOT_FOUND);
    }

    return {
      message: 'Le livre a été récupéré avec succès!',
      content: foundBook,
      status: HttpStatus.OK,
    }
  }

  @Put(':id')
  public async update(@Param('id', ParseIntPipe) id: number, @Body() updateBookDto: CreateUpdateBookDto):Promise<CustomResponseI> {
    await this.bookService.update(+id, updateBookDto);
    return {
      message: `Le livre avec le '${updateBookDto.titre}' a été modifié avec succès!`,
      content: updateBookDto,
      status: HttpStatus.OK,
    }
  }

  @Delete(':id')
  public async remove(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {
    const foundBook = await this.bookService.findOne(+id)
    await this.bookService.remove(+id);
    return {
      message: `Le livre avec le id: '${id}' a été supprimé avec succès!`,
      content: foundBook,
      status: HttpStatus.OK,
    }
  }
}
