import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateUpdateBookDto {

    @IsString()
    @IsNotEmpty()
    titre: string;

    @IsString()
    resume: string;

    @IsOptional()
    @IsNumber()
    author_id: number;

    @IsOptional()
    @IsNumber()
    category_id: number;

}
