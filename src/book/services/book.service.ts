import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUpdateBookDto } from '../dto/create-update-book.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from '../entities/book.entity';
import { DataSource, Repository } from 'typeorm';
import { BookI } from '../interfaces/book.interface';
import { AuthorEntity } from 'src/author/entities/author.entity';
import { CategoryEntity } from 'src/category/entities/category.entity';

@Injectable()
export class BookService {

  constructor(
    private dataSource: DataSource,
    @InjectRepository(AuthorEntity)
    private readonly authorRepository: Repository<AuthorEntity>,
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) { }

  public async create(book: CreateUpdateBookDto): Promise<BookI> {


    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      let createdBook: BookI;

      const newBook = this.bookRepository.create(book);
      const author = await this.authorRepository.findOne({ where: { id: book.author_id } });
      const category = await this.categoryRepository.findOne({ where: { id: book.category_id } });
      
      newBook.author = author;
      newBook.categories = [category];
      
      createdBook = await this.bookRepository.save(newBook);

      await queryRunner.commitTransaction();

      return createdBook;

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for creating book is failed', HttpStatus.NO_CONTENT);

    } finally {

      await queryRunner.release();

    }
  }

  public async findAll(): Promise<BookI[]> {
    return await this.bookRepository.find();
  }

  findOne(id: number): Promise<BookI> {
    return this.bookRepository.findOne({ where: { id: id }, relations: { author: true } })
  }

  public async update(id: number, updatedBook: CreateUpdateBookDto) {

    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      const foundedBook = await this.bookRepository.findOne({ where: { id: id } });

      await queryRunner.manager.transaction(async (manager) => {
        const bookRepository = manager.withRepository(this.bookRepository);
        const newBook = this.bookRepository.create(updatedBook);
        const authorRepository = manager.withRepository(this.authorRepository);
        const author = await this.authorRepository.findOne({ where: { id: updatedBook.author_id } });

        foundedBook.author = author;
        foundedBook.titre = newBook.titre;
        foundedBook.resume = newBook.resume


        return await this.bookRepository.update(foundedBook.id, foundedBook);
      })

      await queryRunner.commitTransaction();

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for creating author is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }
  }

  public async remove(id: number): Promise<any> {

    const foundedBook = await this.bookRepository.findOne({ where: { id: id } });

    if (foundedBook) {
      return await this.bookRepository.delete(foundedBook.id);
    } else {
      throw new HttpException(`Le livre n'existe plus`, HttpStatus.NOT_FOUND);
    }
  }

}
