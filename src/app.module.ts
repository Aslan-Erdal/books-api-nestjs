import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { CategoryModule } from './category/category.module';
import { AuthorModule } from './author/author.module';
import { BookModule } from './book/book.module';
import { BorrowModule } from './borrow/borrow.module';
import { AuthModule } from './auth/auth.module';
import { AuthMiddleware } from './auth/middleware/auth.middleware';
import { ConfigModule } from '@nestjs/config';
import { BookEntity } from './book/entities/book.entity';
@Module({
  imports: [
    ConfigModule.forRoot({isGlobal: true}),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'fafa',
      database: 'books',
      entities: [],
      autoLoadEntities: true,
      synchronize: true,
    }),
    UserModule,
    CategoryModule,
    AuthorModule,
    BookModule,
    BorrowModule,
    AuthModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
    .apply(AuthMiddleware)
    .exclude(
      {
        path: 'users',
        method: RequestMethod.POST
      },
      {
        path: 'users/login',
        method: RequestMethod.POST
      }
    ).forRoutes('');
  }
}
