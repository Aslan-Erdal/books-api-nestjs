import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUser } from 'src/user/interfaces/user.interface';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(private jwtService: JwtService) { }

    // Method for generate a Jwt token
    async generateJwt(user: IUser): Promise<string> {
        return this.jwtService.signAsync({ user });
    }

    // Method for hashing password 
    async hashPassword(password: string): Promise<string> {
        return bcrypt.hash(password, 12);
    }

    // Method for comparePasswords
    async comparePasswords(password: string, storedPasswordhash: string): Promise<any> {
        return bcrypt.compare(password, storedPasswordhash);
    }

    // Method for verify Jwt
    verifyJwt(jwt: string): Promise<any> {
        return this.jwtService.verifyAsync(jwt);
    }
}
