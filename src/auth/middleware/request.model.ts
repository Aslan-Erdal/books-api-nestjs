import { IUser } from "src/user/interfaces/user.interface";

export interface RequestModel {
    user: IUser;
    headers: any;
}