import { HttpException, HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { UserService } from 'src/user/services/user.service';
import { RequestModel } from './request.model';
import { NextFunction, Response } from 'express';
import { IUser } from 'src/user/interfaces/user.interface';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  async use(request: RequestModel, response: Response, next: NextFunction) {
    try {
      const tokenArray: string[] = request.headers['authorization'].split(' ');
      const decodedToken = await this.authService.verifyJwt(tokenArray[1]);

      const user: IUser = await this.userService.getOneById(decodedToken.user.id);

      if (user) {
        request.user = user;

        next();
      } else {
        throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
      }
    } catch {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }
}
