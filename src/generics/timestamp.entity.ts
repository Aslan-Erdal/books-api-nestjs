import { CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from "typeorm";

export class TimestampEntities {

    @CreateDateColumn({ type: 'timestamp', nullable: true, update: false })
    createdDate: Date;

    @UpdateDateColumn({ type: 'timestamp', nullable: true })
    updatedDate: Date;

    @DeleteDateColumn({ type: 'timestamp', nullable: true })
    deletedDate: Date;
    
}