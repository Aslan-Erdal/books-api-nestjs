import { Controller, Get, Post, Body, Param, Delete, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { BorrowService } from '../services/borrow.service';
import { CustomResponseI } from 'src/book/interfaces/book.interface';
import { BorrowBookDto, UpdateBorrowBookDto } from '../dto/borrow-book.dto';

@Controller('borrow')
export class BorrowController {
  constructor(private readonly borrowService: BorrowService) {}

  @Post()
  public async create(@Body() borrowBookDto: BorrowBookDto): Promise<CustomResponseI> {
     this.borrowService.createBorrowBook(borrowBookDto);

    return {
      message: `Le livre a été emprunté avec succès`,
      status: HttpStatus.CREATED,
    }
  }

  @Get()
  public async findAll(): Promise<CustomResponseI> {
    const allBorrows = await this.borrowService.findAll();
    return {
      message: `Tous emprunts ont été récupérés avec succès`,
      content: allBorrows,
      status: HttpStatus.OK,
    }
  }

  @Get(':id')
  public findOne(@Param('id') id: string) {
    return this.borrowService.findOne(+id);
  }

  @Put(':id')
  public async update(@Param('id', ParseIntPipe) id: number, @Body() updateBorrowDto: UpdateBorrowBookDto): Promise<CustomResponseI> {
    const updatedBorrow = await this.borrowService.update(id, updateBorrowDto);

    const returnBorrow = await this.borrowService.findOne(id);

    return {
      message: `Le livre: '${ returnBorrow.books.titre }' est retourné a la bibliothèque!`,
      content: updatedBorrow,
      status: HttpStatus.OK,
    }
  }

  @Delete(':id')
  public async remove(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {

     await this.borrowService.remove(id);

    return {
      message: `L'emprunt a été supprimé avec succès!`,
      status: HttpStatus.OK,
    }
  }
}
