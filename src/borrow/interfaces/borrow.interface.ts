export interface BorrowI {
    date_emprunt?: Date;
    date_retour?: Date;
    book_id?: number;
    user_id?: number;
}