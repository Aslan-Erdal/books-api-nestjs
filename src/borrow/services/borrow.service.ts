import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { BorrowBookDto, UpdateBorrowBookDto } from '../dto/borrow-book.dto';
import { BorrowI } from '../interfaces/borrow.interface';
import { DataSource, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from 'src/book/entities/book.entity';
import { UserEntity } from 'src/user/entities/abonne.entity';
import { BorrowEntity } from '../entities/borrow.entity';

@Injectable()
export class BorrowService {
  constructor(
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(BorrowEntity)
    private readonly borrowRepository: Repository<BorrowEntity>,
    private dataSource: DataSource,
  ) { }

  // Create a service for borrowing a book;
  public async createBorrowBook(borrowUserBook: BorrowBookDto): Promise<BorrowI> {
    const queryRunner = await this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      const newBorrow = await this.borrowRepository.create(borrowUserBook);

      const foundBook = await this.bookRepository.findOne({
        where: { id: borrowUserBook.book_id },
      });
      const foundUser = await this.userRepository.findOne({
        where: { id: borrowUserBook.user_id },
      });

      newBorrow.books = foundBook;
      newBorrow.abonnes = foundUser;

      return await this.borrowRepository.save(newBorrow);

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for creating book is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }

  }

  public findAll() {
    return this.borrowRepository.find({ relations: { abonnes: true, books: true } });
  }

  public findOne(id: number) {
    return this.borrowRepository.findOne({ where: { id: id}, relations: { books: true } });
  }

  public async update(id: number, updateBorrowDto: UpdateBorrowBookDto): Promise<UpdateBorrowBookDto> {
    const queryRunner = await this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      const updateBorrow = await this.borrowRepository.findOne({ where: { id: id} });

      const newDate = new Date(updateBorrowDto.date_retour)
      updateBorrow.date_retour = newDate;

     await this.borrowRepository.update(id, updateBorrow);
     return updateBorrow;

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for creating book is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }
  }

  public async remove(id: number): Promise<any> {
    const foundedBorrow = await this.borrowRepository.findOne({ where: { id: id} });
    
    if (!foundedBorrow) {
      throw new HttpException('L\'emprunt est introuvable', HttpStatus.NOT_FOUND);
    }

    return this.borrowRepository.delete(foundedBorrow.id);
  }
}
