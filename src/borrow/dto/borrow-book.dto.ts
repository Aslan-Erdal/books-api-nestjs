import { IsDate, IsNumber, IsOptional } from "class-validator";

export class BorrowBookDto {

    @IsOptional()
    @IsDate()
    date_emprunt: Date;

    @IsOptional()
    @IsDate()
    date_retour: Date;

    @IsNumber()
    user_id: number;

    @IsNumber()
    book_id: number;

}

export type UpdateBorrowBookDto = Partial<BorrowBookDto>