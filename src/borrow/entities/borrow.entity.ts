import { BookEntity } from "src/book/entities/book.entity";
import { TimestampEntities } from "src/generics/timestamp.entity";
import { UserEntity } from "src/user/entities/abonne.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity('emprunt')
export class BorrowEntity extends TimestampEntities{

    @PrimaryGeneratedColumn({
        type: "int",
    })
    id: number;

    @CreateDateColumn({name: 'date_emprunt', type: 'timestamp' })
    date_emprunt: Date;

    @Column({name: 'date_retour', type: 'timestamp', nullable: true })
    date_retour: Date;

    // @PrimaryColumn({ name: 'abonne_id'})
    // abonneId: number;

    // @PrimaryColumn({ name : 'book_id'})
    // bookId: number;

    @ManyToOne( () => UserEntity,
        abonnes => abonnes.borrow,
        {
            cascade: ['insert', 'update'],
            nullable: true,
        })
    @JoinColumn({ name: 'abonne_id' })
    abonnes: UserEntity;

    @ManyToOne( () => BookEntity,
        books => books.borrow,
        {
            cascade: ['insert', 'update'],
        })
    @JoinColumn({ name: 'book_id' })
    books: BookEntity
}
