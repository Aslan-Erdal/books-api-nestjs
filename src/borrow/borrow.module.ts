import { Module } from '@nestjs/common';
import { BorrowService } from './services/borrow.service';
import { BorrowController } from './controller/borrow.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BorrowEntity } from './entities/borrow.entity';
import { BookEntity } from 'src/book/entities/book.entity';
import { UserEntity } from 'src/user/entities/abonne.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([BorrowEntity, BookEntity, UserEntity])
  ],
  controllers: [BorrowController],
  providers: [BorrowService]
})
export class BorrowModule {}
