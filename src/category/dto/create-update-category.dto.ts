import { IsArray, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateCategoryDto {

    @IsNotEmpty()
    @IsString()
    libelle: string;

    @IsOptional()
    @IsString()
    @IsArray()
    mots_cles: string[];
}

export type UpdateCategoryDto = Partial<CreateCategoryDto>;
