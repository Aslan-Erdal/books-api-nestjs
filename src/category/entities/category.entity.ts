import { BookEntity } from "src/book/entities/book.entity";
import { TimestampEntities } from "src/generics/timestamp.entity";
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'category'})
export class CategoryEntity extends TimestampEntities {
    @PrimaryGeneratedColumn({
        type: "int",
        name: "category_id",
        primaryKeyConstraintName: 'category_id'
    })
    id: number;

    @Column({name: 'libelle', nullable: true})
    libelle: string;

    @Column("varchar",{name: 'mots_cles', nullable: true, array: true})
    mots_clés: string[];

    @ManyToMany(
        () => BookEntity,
        books => books.categories,
        {
            nullable: true,
            cascade: true,
        }
    )
    books: BookEntity[];
}
