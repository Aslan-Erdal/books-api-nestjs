import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, ParseIntPipe, HttpException, Put } from '@nestjs/common';
import { CategoryService } from '../services/category.service';
import { CreateCategoryDto } from '../dto/create-update-category.dto';
import { UpdateCategoryDto } from '../dto/create-update-category.dto';
import { CustomResponseI } from 'src/book/interfaces/book.interface';

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) { }

  @Post()
  async create(@Body() createCategoryDto: CreateCategoryDto): Promise<CustomResponseI> {
    await this.categoryService.create(createCategoryDto);

    return {
      message: 'La category a été crée avec succès',
      content: createCategoryDto,
      status: HttpStatus.CREATED,
    }
  }

  @Get()
  public findAll() {
    return this.categoryService.findAll();
  }

  @Get(':id')
  public async findOne(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {

    const category = await this.categoryService.findOne(id);

    if (!category) {
      throw new HttpException('La category est introuvable', HttpStatus.NOT_FOUND);
    }

    return {
      message: 'La category a récupéré avec succès',
      content: category,
      status: HttpStatus.OK,
    }

  }

  @Put(':id')
  public async update(@Param('id', ParseIntPipe) id: number, @Body() updateCategoryDto: UpdateCategoryDto): Promise<CustomResponseI> {
    
    this.categoryService.update(id, updateCategoryDto);

    return {
      message: 'La category a été modifié avec succès',
      content: updateCategoryDto,
      status: HttpStatus.OK,
    }

  }

  @Delete(':id')
  public async remove(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {

    const removedCategory = await this.categoryService.remove(id);
    
    if (!removedCategory) {
      throw new HttpException('La category est introuvable', HttpStatus.NOT_FOUND);
    }

    return {
      message: 'La category a été supprimé avec succès',
      content: removedCategory,
      status: HttpStatus.OK,
    }

  }
}
