import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCategoryDto } from '../dto/create-update-category.dto';
import { UpdateCategoryDto } from '../dto/create-update-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '../entities/category.entity';
import { DataSource, Repository } from 'typeorm';
import { CategoryI } from '../interfaces/category.interface';

@Injectable()
export class CategoryService {

  constructor(
    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
    private dataSource: DataSource,
  ) { }

  public async create(createCategoryDto: CreateCategoryDto): Promise<CategoryI> {
    const existingCategory = await this.categoryRepository.findOneBy({ libelle: createCategoryDto.libelle});

    if (existingCategory) {
      throw new HttpException('La category existe!', HttpStatus.CONFLICT);
    }
    
    const createdCategory = this.categoryRepository.create(createCategoryDto);
    return await this.categoryRepository.save(createdCategory);
  }

  public findAll() {
    return this.categoryRepository.find();
  }

  public findOne(id: number): Promise<CategoryI> {
    return this.categoryRepository.findOne({ where: { id: id}, relations: { books: true}})
  }

  public async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const existingCategory = await this.categoryRepository.findOneBy({ id: id });
    existingCategory.libelle = updateCategoryDto.libelle;
    return await this.categoryRepository.update(id, existingCategory);
  }

  public async remove(id: number): Promise<any> {
    
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

        const foundedBook = await this.categoryRepository.findOne({ where: { id: id } });

        await this.categoryRepository.remove(foundedBook);

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for deleting category is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }
    return this.categoryRepository.delete(id)
  }
}
