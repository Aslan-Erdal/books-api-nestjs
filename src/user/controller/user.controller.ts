import { Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Post } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { createOrLoginUserDto } from '../dto/create-login-user.dto';
import { ILoginResponse, IUser } from '../interfaces/user.interface';
import { CustomResponseI } from 'src/book/interfaces/book.interface';

@Controller('users')
export class UserController {

    constructor(
        private readonly userService: UserService
    ) { }


    @Post()
    public async create(
        @Body() createUserDto: createOrLoginUserDto
    ): Promise<IUser> {
        return this.userService.create(createUserDto);
    } 

    @Get(':id')
    public async getUserWithBorrows(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {
        console.log(id);
        
        const user = await this.userService.findUserWithBorrowedBooks(id);

        return {
            message: 'L\'abonné a été récupéré avec les livres empruntés!',
            content: user,
            status: HttpStatus.OK,
        }
    }

    @Post('login')
    public async login(
        @Body() createOrLoginUserDto: createOrLoginUserDto
    ): Promise<ILoginResponse> {
        const jwt: string = await this.userService.login(createOrLoginUserDto);

        return {
            access_token: jwt,
            token_type: 'JWT',
            expires_in: 5000
        }
    }

    @Get()
    public async findAllUsers(): Promise<CustomResponseI> {
        // TODO : Plus tard p-e, faire la pagination...
        const abonnes = await this.userService.findAll();

        return {
            message: 'Tous les abonnés ont été récupérés!',
            content: abonnes,
            status: HttpStatus.OK,
        }
    }
}
