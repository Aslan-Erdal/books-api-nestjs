import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/abonne.entity';
import { DataSource, Repository } from 'typeorm';
import { AuthService } from 'src/auth/services/auth.service';
import { IUser } from '../interfaces/user.interface';
import { UserDto, createOrLoginUserDto } from '../dto/create-login-user.dto';
import { BookEntity } from 'src/book/entities/book.entity';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private authService: AuthService,
        @InjectRepository(BookEntity)
        private readonly bookRepository: Repository<BookEntity>,
        private dataSource: DataSource,
    ) { }

    public async create(newUser: IUser): Promise<IUser> {
        // check is username exists
        const usernamExists: boolean = await this.usernameExists(newUser.username);
        if(!usernamExists) {
            const passwordHash: string = await this.authService.hashPassword(newUser.password);

            newUser.password = passwordHash;
            newUser.username = newUser.username.toLowerCase();

            const user = await this.userRepository.save(
                this.userRepository.create(newUser),
            );
            return this.findOne(user.id);
        } else {
            throw new HttpException(
                ` Le nom d'utilisateur est déja pris.`,
                HttpStatus.CONFLICT,
            )
        }
    }

    public async login(user: IUser): Promise<string> {
        const foundUser: IUser = await this.findByUsername(user.username);

        if (foundUser) {
            const passwordMatching: boolean = await this.authService.comparePasswords(user.password, foundUser.password);

            if (passwordMatching == true) {
                const payload: IUser = await this.findOne(foundUser.id);

                return this.authService.generateJwt(payload);
            } else {
                throw new HttpException(
                    'Les informations sont erronées',
                    HttpStatus.UNAUTHORIZED,
                )
            }
        } else {
            throw new HttpException( `L'utilisateur n'a pas été trouvé`, HttpStatus.NOT_FOUND );
        }

    }

    public findAll(): Promise<IUser[]> {
        return this.userRepository.find({ relations: { borrow: { books: true } }});
    }

    // find a User with borrowed book
    public async findUserWithBorrowedBooks(id: number): Promise<IUser> {
        const foundedUser = await this.userRepository.findOne({
            where: { id: id }, relations: { borrow: { books: true } }
        });

        if (!foundedUser) {
            throw new HttpException('L\'utilisateur est introuvable', HttpStatus.NOT_FOUND);
        }

        return foundedUser;
    }


    // find by username
    private async findByUsername(username: string): Promise<IUser> {
        return this.userRepository.findOne({
            where: { username },
            select: ['id', 'username', 'password'],
        })
    }

    // get an user by id or Fail;
    public async getOneById(id: number): Promise<IUser> {
        return this.userRepository.findOneByOrFail({ id: id });
    }

    // find user by <param>
    private async findOne(id: number): Promise<IUser> {
        return this.userRepository.findOne({
            where: { id },
        })
    }

    // check if username exists
    private async usernameExists(username: string): Promise<boolean> {
        const user = await this.userRepository.findOne({
            where: { username }
        })
        return !!user;
    }
}
