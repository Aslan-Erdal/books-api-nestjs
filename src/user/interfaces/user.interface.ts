export interface IUser {
    id?: number;
    prenom?: string;
    nom?: string;
    username: string;
    password?: string;
    naissance?: Date;
}

export interface ILoginResponse {
    access_token: string;
    token_type: string;
    expires_in: number;
}