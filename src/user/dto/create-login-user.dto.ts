import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class createOrLoginUserDto{

    @IsString()
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;

    // @IsOptional()
    // @IsNumber()
    // book_id: number;

}

export type UserDto = Partial<createOrLoginUserDto>;