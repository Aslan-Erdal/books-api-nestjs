import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { UserEntity } from './entities/abonne.entity';
import { UserService } from './services/user.service';
import { UserController } from './controller/user.controller';
import { BookEntity } from 'src/book/entities/book.entity';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([UserEntity, BookEntity])
    ],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
