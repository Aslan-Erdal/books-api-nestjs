import { BorrowEntity } from "src/borrow/entities/borrow.entity";
import { TimestampEntities } from "src/generics/timestamp.entity";
import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('abonne')
export class UserEntity extends TimestampEntities{

    @PrimaryGeneratedColumn({
        type: "int",
        name: 'id',
        primaryKeyConstraintName: 'id',
    })
    id: number;

    @Column({
        name: 'prenom',
        nullable: true,
    })
    prenom: string;

    @Column({
        name: 'nom',
        nullable: true
    })
    nom: string;

    @Column({
        name: 'username',
        unique: true,
        nullable: false
    })
    username: string;

    @Column({
        name: 'password',
        select: false,
        nullable: false,
    })
    password: string;

    @Column({ name: 'naissance', type: 'timestamp', nullable: true })
    naissance: Date;


    @OneToMany(() => BorrowEntity, (borrow) => borrow.abonnes)
    borrow: BorrowEntity[]

    @BeforeInsert()
    @BeforeUpdate()
    emailToLowerCase() {
        this.username = this.username.toLowerCase().trim();
    }

}