
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateAuthorDto, UpdateAuthorDto } from '../dto/create-update-author.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorEntity } from '../entities/author.entity';
import { DataSource, Repository } from 'typeorm';
import { AuthorI } from '../interfaces/author.interface';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(AuthorEntity)
    private readonly authorRepository: Repository<AuthorEntity>,
    private dataSource: DataSource,
  ) { }


  public async create(createAuthorDto: CreateAuthorDto): Promise<AuthorI> {

    const findUserByName = await this.authorRepository.findOne({ where: { nom: createAuthorDto.nom } });
    const findUserBySurname = await this.authorRepository.findOne({ where: { prenom: createAuthorDto.prenom } });

    if (findUserByName && findUserBySurname) {
      throw new HttpException('L\'Auteur de ce nom et prenom existe dans la base de données', HttpStatus.CONFLICT)
    }

    const newAuthor = await this.authorRepository.create(createAuthorDto)
    return await this.authorRepository.save(newAuthor);

  }

  public findAll(): Promise<AuthorI[]> {
    return this.authorRepository.find();
  }

  public findOne(id: number): Promise<AuthorI> {
    return this.authorRepository.findOne({ where: { id: id }, relations: { livres: true } })
  }

  public async update(id: number, updateAuthorDto: UpdateAuthorDto) {
    // await this.bookRepository.create(createUpdateBookDto);
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      const foundedAuthor = await this.authorRepository.findOneBy({ id: id });

      if (!foundedAuthor) {
        throw new HttpException('Author est introuvable', HttpStatus.NOT_FOUND);
      }

      return await this.authorRepository.update(id, updateAuthorDto);

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for updating author is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }
  }

  public async remove(id: number): Promise<any> {

    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();

    await queryRunner.startTransaction();

    try {

      await this.authorRepository.delete(id);

    } catch {

      await queryRunner.rollbackTransaction();
      throw new HttpException('Transaction for deleting book is failed', HttpStatus.NO_CONTENT);

    } finally {
      await queryRunner.release();
    }

    // const foundedAuthor = await this.authorRepository.findOne({ where: { id: id } });
    // if (foundedAuthor) {
    // } else {
    //   throw new HttpException(`L\'author n'existe plus`, HttpStatus.NOT_FOUND);
    // }
  }
}

