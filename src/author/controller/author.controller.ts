
import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, HttpStatus, HttpException, Put } from '@nestjs/common';
import { AuthorService } from '../services/author.service';
import { CreateAuthorDto, UpdateAuthorDto } from '../dto/create-update-author.dto';
import { CustomResponseI } from 'src/book/interfaces/book.interface';

@Controller('authors')
export class AuthorController {
  constructor(private readonly authorService: AuthorService,) { }

  @Post()
  public async create(@Body() createAuthorDto: CreateAuthorDto): Promise<CustomResponseI> {
    const createdAuthor = await this.authorService.create(createAuthorDto);
    return {
      message: 'Author a été crée avec succès',
      content: createdAuthor,
      status: HttpStatus.CREATED,
    }
  }

  @Get()
  public async findAll(): Promise<CustomResponseI> {
    const authors = await this.authorService.findAll();
    return {
      message: 'Les authors ont été récupérés avec succès',
      content: authors,
      status: HttpStatus.OK,
    }
  }

  @Get(':id')
  public async findOne(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {
    const author = await this.authorService.findOne(+id);
    if (author === null) {
      throw new HttpException('Author est introuvable', HttpStatus.NOT_FOUND);
    }
    return {
      message: 'L\'author a été récupéré avec succès',
      content: author,
      status: HttpStatus.OK,
    }
  }

  @Put(':id')
  public async update(@Param('id', ParseIntPipe) id: number, @Body() updateAuthorDto: UpdateAuthorDto): Promise<CustomResponseI> {
    const author = await this.authorService.update(id, updateAuthorDto);
    return {
      message: 'L\'author a été modifié avec succès',
      content: author,
      status: HttpStatus.OK,
    }
  }

  @Delete(':id')
  public async remove(@Param('id', ParseIntPipe) id: number): Promise<CustomResponseI> {
    // TODO : check if found in db and remove it;
    const author = await this.authorService.findOne(id);

    try {

      if (!author) {
        throw new HttpException('Author est introuvable', HttpStatus.NOT_FOUND);
      }

      await this.authorService.remove(id);

      return {
        message: 'L\'author a été supprimé avec succès',
        content: author,
        status: HttpStatus.OK,
      }
    } catch {
      return {
        message: 'Le serveur est introuvable',
        status: HttpStatus.INTERNAL_SERVER_ERROR
      }
    }

  }
}