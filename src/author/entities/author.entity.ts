import { BookEntity } from "src/book/entities/book.entity";
import { TimestampEntities } from "src/generics/timestamp.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'author' })
export class AuthorEntity extends TimestampEntities {
    @PrimaryGeneratedColumn({
        type: "int",
        name: 'author_id',
        primaryKeyConstraintName: 'author_id'
    })
    id: number;

    @Column({
        name: 'prenom',
        nullable: true,
    })
    prenom: string;

    @Column({
        name: 'nom',
        nullable: false,
    })
    nom: string;

    @Column({
        name: 'bio',
        nullable: true
    })
    bio: string;

    @OneToMany(type => BookEntity, (book) => book.author,
        {
            cascade: true,
            onDelete: "CASCADE"
        })
    livres: BookEntity[]

    @Column({ name: 'naissance', type: 'timestamp', nullable: true })
    naissance: Date;

}
