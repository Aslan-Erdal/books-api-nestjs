import { IsDate, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateAuthorDto {
    @IsNotEmpty()
    @IsString()
    prenom: string;

    @IsNotEmpty()
    @IsString()
    nom: string;

    @IsOptional()
    @IsString()
    bio: string;

    @IsOptional()
    @IsDate()
    naissance: Date;
}

export type UpdateAuthorDto = Partial<CreateAuthorDto>;