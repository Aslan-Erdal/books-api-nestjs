export interface AuthorI {
    prenom: string;
    nom: string;
    bio?: string;
    naissance?: Date;
}